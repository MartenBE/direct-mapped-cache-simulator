# Direct Mapped Cache simulator

This program allows you to simulate how a direct mapped cache works.
It will ask for memory addresses which the cpu wants to check. You can stop the program by using `Ctrl+C`.

Usage:

```
cache.exe CACHESIZE RULESIZE MEMORYFILE
        CACHESIZE The total size of the cache in bytes
        RULESIZE The size of a single rule in the cache in bytes
        MEMORYFILE The filename containing the contents of the main memory in hexadecimal values separated with whitespace
```

An example memory file `example.txt`:

```
47 46 45 44 43 42 41 40
E7 E6 E5 E4 E3 E2 E1 E0
D7 D6 D5 D4 D3 D2 D1 D0
DF DE DD DC DB DA D9 D8
9F 9E 9D 9C 9B 9A 99 98
BF BE BD BC BB BA B9 B8
C7 C6 C5 C4 C3 C2 C1 C0
A7 A6 A5 A4 A3 A2 A1 A0
F7 F6 F5 F4 F3 F2 F1 F0
37 36 35 34 33 32 31 30
17 16 15 14 13 12 11 10
2F 2E 2D 2C 2B 2A 29 28
1F 1E 1D 1C 1B 1A 19 18
27 26 25 24 23 22 21 20
0F 0E 0D 0C 0B 0A 09 08
07 06 05 04 03 02 01 00
```

An example memory file `example2.txt`:

```
11 22 33 44 
F6 F7 F8 F9
F2 F3 F4 F5
E7 E8 E9 F1
E3 E4 E5 E6
D8 D9 E1 E2
D4 D5 D6 D7
C9 D1 D2 D3
C5 C6 C7 C8
C1 C2 C3 C4
B6 B7 B8 B9
B2 B3 B4 B5
A7 A8 A9 B1
A3 A4 A5 A6
EE FF A1 A2
AA BB CC DD
```

This program can be compiled with `g++` as follows:

```
g++ -std=c++17 main.cpp memory.cpp cache.cpp -o cache
```

It can also be compiled using Visual Studio, Clang, ... as long as C++ 17 is enabled.

Example usage:

```
cache.exe 32 8 example.txt
```

```
Cache:

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 | false | -        | -
00000010 | false | -        | -
00000011 | false | -        | -

Memory:

 Tag     | Index    | Data
---------+----------+------
00000000 | 00000000 | 47 46 45 44 43 42 41 40
00000000 | 00000001 | e7 e6 e5 e4 e3 e2 e1 e0
00000000 | 00000010 | d7 d6 d5 d4 d3 d2 d1 d0
00000000 | 00000011 | df de dd dc db da d9 d8
00000001 | 00000000 | 9f 9e 9d 9c 9b 9a 99 98
00000001 | 00000001 | bf be bd bc bb ba b9 b8
00000001 | 00000010 | c7 c6 c5 c4 c3 c2 c1 c0
00000001 | 00000011 | a7 a6 a5 a4 a3 a2 a1 a0
00000010 | 00000000 | f7 f6 f5 f4 f3 f2 f1 f0
00000010 | 00000001 | 37 36 35 34 33 32 31 30
00000010 | 00000010 | 17 16 15 14 13 12 11 10
00000010 | 00000011 | 2f 2e 2d 2c 2b 2a 29 28
00000011 | 00000000 | 1f 1e 1d 1c 1b 1a 19 18
00000011 | 00000001 | 27 26 25 24 23 22 21 20
00000011 | 00000010 | 0f 0e 0d 0c 0b 0a 09 08
00000011 | 00000011 | 07 06 05 04 03 02 01 00

Give a memory address >: 4a
Asked for 4a (01001010)
Tag: 2 (00000010)
Index: 1 (00000001)
Byte offset: 2 (00000010)
Cache miss! Copying the rule to the cache ...

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 |  true | 00000010 | 37 36 35 34 33 32 31 30
00000010 | false | -        | -
00000011 | false | -        | -

Give a memory address >: 63
Asked for 63 (01100011)
Tag: 3 (00000011)
Index: 0 (00000000)
Byte offset: 3 (00000011)
Cache miss! Copying the rule to the cache ...

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 |  true | 00000011 | 1f 1e 1d 1c 1b 1a 19 18
00000001 |  true | 00000010 | 37 36 35 34 33 32 31 30
00000010 | false | -        | -
00000011 | false | -        | -

Give a memory address >:
Ctrl+C
```

Another example:

```
cache.exe 16 4 example2.txt 
```

```
Cache:

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 | false | -        | -
00000010 | false | -        | -
00000011 | false | -        | -

Memory:

 Tag     | Index    | Data
---------+----------+------
00000000 | 00000000 | 11 22 33 44
00000000 | 00000001 | f6 f7 f8 f9
00000000 | 00000010 | f2 f3 f4 f5
00000000 | 00000011 | e7 e8 e9 f1
00000001 | 00000000 | e3 e4 e5 e6
00000001 | 00000001 | d8 d9 e1 e2
00000001 | 00000010 | d4 d5 d6 d7
00000001 | 00000011 | c9 d1 d2 d3
00000010 | 00000000 | c5 c6 c7 c8
00000010 | 00000001 | c1 c2 c3 c4
00000010 | 00000010 | b6 b7 b8 b9
00000010 | 00000011 | b2 b3 b4 b5
00000011 | 00000000 | a7 a8 a9 b1
00000011 | 00000001 | a3 a4 a5 a6
00000011 | 00000010 | ee ff a1 a2
00000011 | 00000011 | aa bb cc dd

Give a memory address >: 19
Asked for 19 (00011001)
Tag: 1 (00000001)
Index: 2 (00000010)
Byte offset: 1 (00000001)
Cache miss! Copying the rule to the cache ...

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 | false | -        | -
00000010 |  true | 00000001 | d4 d5 d6 d7
00000011 | false | -        | -

Give a memory address >: 3f
Asked for 3f (00111111)
Tag: 3 (00000011)
Index: 3 (00000011)
Byte offset: 3 (00000011)
Cache miss! Copying the rule to the cache ...

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 | false | -        | -
00000010 |  true | 00000001 | d4 d5 d6 d7
00000011 |  true | 00000011 | aa bb cc dd

Give a memory address >: 2a
Asked for 2a (00101010)
Tag: 2 (00000010)
Index: 2 (00000010)
Byte offset: 2 (00000010)
Cache miss! Copying the rule to the cache ...

 Index   | Valid | Tag      | Data
---------+-------+----------+------
00000000 | false | -        | -
00000001 | false | -        | -
00000010 |  true | 00000010 | b6 b7 b8 b9
00000011 |  true | 00000011 | aa bb cc dd

Give a memory address >:
Ctrl+C
```