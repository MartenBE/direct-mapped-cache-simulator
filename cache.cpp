#include "cache.h"

#include <bitset>
#include <cmath>
#include <iomanip>
#include <iostream>

Cache::Cache(int size, int rule_size, Memory* memory) : size{size}, rule_size{rule_size}, memory{memory}
{
    amount_rules = (size / rule_size);
    amount_cache_blocks_memory = (memory->size() / size);

    amount_bits_bytes_offset = static_cast<int>(std::log2(rule_size));
    amount_bits_index = static_cast<int>(std::log2(amount_rules));
    amount_bits_tag = static_cast<int>(std::log2(amount_cache_blocks_memory));

    rules.reserve(amount_rules);
    for (int i = 0; i < amount_rules; i++)
    {
        rules.emplace_back(rule_size);
    }
}

void Cache::ask(uint8_t address)
{
    int tag = retrieve_tag(address);
    int index = retrieve_index(address);
    int byte_offset = retrieve_byte_offset(address);

    std::cout << "Tag: " << tag << " (" << std::bitset<8>(tag) << ")" << std::endl;
    std::cout << "Index: " << index << " (" << std::bitset<8>(index) << ")" << std::endl;
    std::cout << "Byte offset: " << byte_offset << " (" << std::bitset<8>(byte_offset) << ")" << std::endl;

    if (is_present(tag, index))
    {
        std::cout << "Cache hit!" << std::endl;
    }
    else
    {
        std::cout << "Cache miss! Copying the rule to the cache ..." << std::endl;
        copy_from_memory(tag, index);
    }
}

bool Cache::is_present(int tag, int index) const
{
    return !(!rules[index].valid || (rules[index].tag != tag));
}

void Cache::copy_from_memory(int tag, int index)
{
    rules[index].valid = true;
    rules[index].tag = tag;

    int first_address = translate_address(tag, index, 0);
    for (int i = 0; i < rule_size; i++)
    {
        rules[index].contents[i] = memory->get(first_address + i);
    }
}

int Cache::retrieve_byte_offset(uint8_t address) const
{
    std::bitset<8> mask = 0;

    for (int i = 0; i < amount_bits_bytes_offset; i++)
    {
        mask.set(i);
    }

    auto result = (std::bitset<8>(address) & mask);

    return result.to_ulong();
}

int Cache::retrieve_index(uint8_t address) const
{
    std::bitset<8> mask = 0;

    for (int i = 0; i < amount_bits_index; i++)
    {
        mask.set(amount_bits_bytes_offset + i);
    }

    auto relevant_bits = (std::bitset<8>(address) & mask);
    auto result = relevant_bits >> amount_bits_bytes_offset;

    return result.to_ulong();
}

int Cache::retrieve_tag(uint8_t address) const
{
    std::bitset<8> mask = 0;

    int offset = (amount_bits_bytes_offset + amount_bits_index);

    for (int i = 0; i < amount_bits_tag; i++)
    {
        mask.set(offset + i);
    }

    auto relevant_bits = (std::bitset<8>(address) & mask);
    auto result = relevant_bits >> offset;

    return result.to_ulong();
}

int Cache::translate_address(int tag, int index, int byte_offset) const
{
    return (tag * size + index * rule_size + byte_offset);
}

void Cache::print_memory() const
{
    std::cout << " Tag     | Index    | Data" << std::endl;
    std::cout << "---------+----------+------" << std::endl;

    for (int tag = 0; tag < amount_cache_blocks_memory; tag++)
    {
        for (int index = 0; index < amount_rules; index++)
        {
            std::cout << std::bitset<8>(tag) << " | " << std::bitset<8>(index) << " | ";

            for (int byte_offset = 0; byte_offset < rule_size; byte_offset++)
            {
                std::cout << std::setw(2) << std::setfill('0') << std::hex
                          << static_cast<unsigned int>(memory->get(translate_address(tag, index, byte_offset))) << " ";
            }

            std::cout << std::endl;
        }
    }
}

void Cache::print() const
{
    std::cout << " Index   | Valid | Tag      | Data" << std::endl;
    std::cout << "---------+-------+----------+------" << std::endl;

    for (int index = 0; index < rules.size(); index++)
    {
        const auto& rule = rules[index];

        std::cout << std::bitset<8>(index) << " | ";
        std::cout << std::setw(5) << std::setfill(' ') << std::boolalpha << rule.valid << " | ";

        if (rule.valid)
        {
            std::cout << std::bitset<8>(rule.tag) << " | ";

            for (const auto& entry : rule.contents)
            {
                std::cout << std::setw(2) << std::setfill('0') << std::hex << static_cast<unsigned int>(entry) << " ";
            }
        }
        else
        {
            std::cout << "-        | -";
        }

        std::cout << std::endl;
    }
}

Cache::Rule::Rule(int rule_size) : contents(rule_size)
{
}