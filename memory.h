#ifndef MEMORY_H
#define MEMORY_H

#include <string>
#include <vector>

class Memory
{
public:
    Memory(int size);
    Memory(const std::string& filename);

    int size() const;
    uint8_t get(int address) const;

private:
    std::vector<uint8_t> contents;
};

#endif