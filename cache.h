#ifndef CACHE_H
#define CACHE_H

#include "memory.h"

class Cache
{
public:
    Cache(int size, int rule_size, Memory* memory);

    void ask(uint8_t address);
    void print_memory() const;
    void print() const;

private:
    class Rule
    {
    public:
        Rule(int rule_size);

        bool valid = false;
        int tag = 0;
        std::vector<uint8_t> contents;
    };

    bool is_present(int tag, int index) const;
    void copy_from_memory(int tag, int index);
    int retrieve_byte_offset(uint8_t address) const;
    int retrieve_index(uint8_t address) const;
    int retrieve_tag(uint8_t address) const;
    int translate_address(int tag, int index, int byte_offset) const;

    int size = 0;
    int rule_size = 0;
    Memory* memory = nullptr;
    int amount_cache_blocks_memory = 0;
    int amount_rules = 0;
    std::vector<Rule> rules;
    int amount_bits_bytes_offset = 0;
    int amount_bits_index = 0;
    int amount_bits_tag = 0;
};

#endif