#include "memory.h"

#include <cassert>
#include <fstream>
#include <random>

Memory::Memory(int size) : contents(size)
{
    std::random_device rd;
    std::mt19937 eng{rd()};
    std::uniform_int_distribution<unsigned int> dist{0, 255};

    for (int i = 0; i < contents.size(); i++)
    {
        contents[i] = static_cast<uint8_t>(dist(eng));
    }
}

Memory::Memory(const std::string& filename)
{
    std::ifstream in{filename};
    assert(in);

    unsigned int memory_byte; // TODO why does uint8_t not work?
    while (in >> std::hex >> memory_byte)
    {
        contents.push_back(static_cast<uint8_t>(memory_byte));
    }
}

int Memory::size() const
{
    return contents.size();
}

uint8_t Memory::get(int address) const
{
    return contents.at(address);
}