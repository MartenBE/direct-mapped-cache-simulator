#include "cache.h"
#include "memory.h"

#include <bitset>
#include <iostream>
#include <memory>

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        std::cout << "Wrong amount of arguments!" << std::endl;
        std::cout << std::endl;
        std::cout << "Usage: cache CACHESIZE RULESIZE MEMORYFILE" << std::endl;
        std::cout << "\t"
                  << "CACHESIZE The total size of the cache in bytes" << std::endl;
        std::cout << "\t"
                  << "RULESIZE The size of a single rule in the cache in bytes" << std::endl;
        std::cout
                << "\t"
                << "MEMORYFILE The filename containing the contents of the main memory in hexadecimal values separated with whitespace"
                << std::endl;

        return 1;
    }

    int cache_size = atoi(argv[1]);
    int cache_rule_size = atoi(argv[2]);
    char* memory_filename = argv[3];

    Memory memory{memory_filename};
    Cache cache(cache_size, cache_rule_size, &memory);

    std::cout << "Cache:" << std::endl << std::endl;
    cache.print();
    std::cout << std::endl;
    std::cout << "Memory:" << std::endl << std::endl;
    cache.print_memory();
    std::cout << std::endl;

    unsigned int address;
    std::string prompt{"Give a memory address >: "};
    std::cout << prompt;
    while (std::cin >> std::hex >> address)
    {
        std::cout << "Asked for " << address << " (" << std::bitset<8>(address) << ")" << std::endl;

        cache.ask(static_cast<uint8_t>(address));

        std::cout << std::endl;
        cache.print();

        std::cout << std::endl << prompt;
    }

    std::cout << "Done ..." << std::endl;

    return 0;
}